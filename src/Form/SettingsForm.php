<?php
namespace Drupal\autoupdate\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure autoupdate settings for this site.
 */
class SettingsForm extends ConfigFormBase {
  /** @var string Config settings */
  const SETTINGS = 'autoupdate.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'autoupdate_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('E-Mail Notification'),
      '#default_value' => ($config->get('email') != '' ? $config->get('email') : ''),
      '#description' => $this->t('If you set here an email address, you wil be notify if the check interval is reached.'),
    ];

    $form['check_interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Check interval (every)'),
      '#options' => array(
        1800 => $this->t('30min'),
        7200 => $this->t('2h'),
        14400 => $this->t('4h'),
        21600 => $this->t('6h'),
        43200 => $this->t('12h'),
        86400 => $this->t('24h'),
      ),
      '#default_value' => ($config->get('check_interval') != '' ? $config->get('check_interval') : '43200'),
      '#description' => $this->t('On this interval the cron checks for new available updates.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('email', $form_state->getValue('email'))
      ->set('check_interval', $form_state->getValue('check_interval'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
