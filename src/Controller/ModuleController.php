<?php
namespace Drupal\autoupdate\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\file\Entity\File;

class ModuleController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    // Check regenerate.
    $regenerate = FALSE;
    if (isset($_GET['regenerate'])) {
      $regenerate = TRUE;
    }

    // Get data from service.
    $data = \Drupal::service('autoupdate.module_manager')->getUpdates(FALSE, $regenerate);
    $generated = $data['generate'];
    $modules = $data['data'];

    // Extract data.
    $version_security = array();
    $version_updates = array();
    $version = array();
    $no_version = array();
    foreach ($modules as $modules) {
      // Transform time.
      $modules['time'] = $modules['time'] ? \Drupal::service('date.formatter')->format($modules['time'], 'short') : NULL;

      // Check used composer setting.
      $composer_setting = $modules['composer_setting'];
      if (strpos($composer_setting, '^') === false AND strpos($composer_setting, '*') === false) {
        // Set message.
        if ($composer_setting == '') {
          $composer_setting = '<div class="tooltip">no version<span class="tooltiptext">' . t('Module is only possible to update if you installed it with composer.') . '</span></div>';
        }
        else {
          $composer_setting = '<div class="tooltip">' . $composer_setting . '<span class="tooltiptext">' . t('You have fix the version number with composer! Not possible with update command to update it.') . '</span></div>';
        }
      }

      // Check security updates.
      if (isset($modules['updates']['security']) AND ($modules['updates']['security'] == 1 OR $modules['updates']['insecure'] == 1)) {
        $version_security[] = array(
          'name' => $modules['name'],
          'version' => $modules['version'],
          'composer_setting' => array('data' => array('#markup' => $composer_setting)),
          'release_version' => $modules['updates']['releases'][0]['orig_version'],
          'release_term' => implode(', ', $modules['updates']['releases'][0]['terms']),
          'composer_path' => $modules['composer_path'],
        );
        continue;
      }

      // Check updates.
      if (count($modules['updates']['releases']) > 0) {
        $version_updates[] = array(
          'name' => $modules['name'],
          'version' => $modules['version'],
          'composer_setting' => array('data' => array('#markup' => $composer_setting)),
          'release_version' => $modules['updates']['releases'][0]['orig_version'],
          'release_term' => implode(', ', $modules['updates']['releases'][0]['terms']),
          'composer_path' => $modules['composer_path'],
        );
        continue;
      }

      // Check version.
      if ($modules['version'] != '') {
        $version[] = array(
          'name' => $modules['name'],
          'version' => $modules['version'],
          'composer_setting' => array('data' => array('#markup' => $composer_setting)),
          'release_version' => NULL,
          'release_term' => NULL,
          'composer_path' => $modules['composer_path'],
        );
        continue;
      }

      // No version found.
      $no_version[] = array(
        'name' => $modules['name'],
        'version' => $modules['version'],
        'composer_setting' => array('data' => array('#markup' => $composer_setting)),
        'release_version' => NULL,
        'release_term' => NULL,
        'composer_path' => $modules['composer_path'],
      );
    }

    // Return header.
    $return[] = array(
      '#markup' => '<p>' . $this->t('Regenerate <a href="?regenerate=yes">module data</a>') .'<br>' . $this->t('Generated, @time', array('@time' => \Drupal::service('date.formatter')->format($generated, 'short'))) . '</p>',
      '#cache' => array('max-age' => 0),
      '#attached' => array(
        'library' => array(
          'autoupdate/autoupdate.modules',
        ),
      ),
    );

    // Return modules with version data and security updates.
    $return[] = [
      '#type' => 'table',
      '#caption' => $this->t('Security updates'),
      '#attributes' => array('id' => 'version_security', 'class' => array('modules')),
      '#header' => [
        array('data' => t('Module'), 'class' => 'module'),
        array('data' => t('Version'), 'class' => 'version'),
        array('data' => t('Composer setting'), 'class' => 'composer_setting'),
        array('data' => t('Release version'), 'class' => 'rversion'),
        array('data' => t('Release term'), 'class' => 'rterm'),
        array('data' => t('Composer path'), 'class' => 'composer_path'),
      ],
      '#rows' => $version_security,
    ];

    // Return modules with version data and updates.
    $return[] = [
      '#type' => 'table',
      '#caption' => $this->t('Updates'),
      '#attributes' => array('id' => 'version_updates', 'class' => array('modules')),
      '#header' => [
        array('data' => t('Module'), 'class' => 'module'),
        array('data' => t('Version'), 'class' => 'version'),
        array('data' => t('Composer setting'), 'class' => 'composer_setting'),
        array('data' => t('Release version'), 'class' => 'rversion'),
        array('data' => t('Release term'), 'class' => 'rterm'),
        array('data' => t('Composer path'), 'class' => 'composer_path'),
      ],
      '#rows' => $version_updates,
    ];

    // Return modules with version data and no updates.
    $return[] = [
      '#type' => 'table',
      '#caption' => $this->t('Up to date'),
      '#attributes' => array('id' => 'version', 'class' => array('modules')),
      '#header' => [
        array('data' => t('Module'), 'class' => 'module'),
        array('data' => t('Version'), 'class' => 'version'),
        array('data' => t('Composer setting'), 'class' => 'composer_setting'),
        array('data' => t('Release version'), 'class' => 'rversion'),
        array('data' => t('Release term'), 'class' => 'rterm'),
        array('data' => t('Composer path'), 'class' => 'composer_path'),
      ],
      '#rows' => $version,
    ];

    // Return no version data.
    $return[] = [
      '#type' => 'table',
      '#caption' => $this->t('No version found'),
      '#attributes' => array('id' => 'no_version', 'class' => array('modules')),
      '#header' => [
        array('data' => t('Module'), 'class' => 'module'),
        array('data' => t('Version'), 'class' => 'version'),
        array('data' => t('Composer setting'), 'class' => 'composer_setting'),
        array('data' => t('Release version'), 'class' => 'rversion'),
        array('data' => t('Release term'), 'class' => 'rterm'),
        array('data' => t('Composer path'), 'class' => 'composer_path'),
      ],
      '#rows' => $no_version,
    ];

    return $return;
  }
}
