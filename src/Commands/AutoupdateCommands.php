<?php

namespace Drupal\autoupdate\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drush\Commands\DrushCommands;

use Consolidation\OutputFormatters\Options\FormatterOptions;
use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Drush\Drush;
use Drush\SiteAlias\SiteAliasManagerAwareInterface;
use Drush\Utils\StringUtils;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 *
 * Example:
 *   - https://gbyte.co/blog/creating-drush-9-commands-and-porting-legacy-commands
 */
class AutoupdateCommands extends DrushCommands {

  /**
   * Get updates for installed modules.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option force
   *   Force check for new updates.
   * @option security
   *   Get only security updates.
   * @option update_string
   *   Get string with modules.
   * @option update_string_version
   *   Get string with modules and version
   * @option format
   *   Get return format (table, json, yaml, csv).
   *
   * @usage autoupdate:updates
   *   Get all module updates.
   * @usage autoupdate:updates --force
   *   Force check for new nupdates.
   * @usage autoupdate:updates --security
   *   Get only module security updates.
   * @usage autoupdate:updates --update_string
   *   Get string with modules.
   * @usage autoupdate:updates --security --update_string
   *   Get string only for security updates.
   *
   * @command autoupdate:updates
   * @aliases autoupdate:updates
   *
   * @field-labels
   *   module: Module
   *   version: Version
   *   install_type: Install type
   *   composer_path: Composer path
   *   composer_setting: Composer settings
   *   security: Security
   *   insecure: Insecure
   *   release_version: Release version
   *   release_terms: Release terms
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields|string
   */
  public function updates($options = ['force' => FALSE, 'security' => FALSE, 'update_string' => FALSE, 'update_string_version' => FALSE, 'format' => 'table']) {
    // Load configuration.
    //$config = \Drupal::config('autoupdate.settings');

    // Get updates.
    $rows = array();
    $composer = array();
    $composer_version = array();
    $updates = \Drupal::service('autoupdate.module_manager')->getUpdates($options['security'], $options['force']);
    foreach ($updates['data'] as $update) {
      // Get values.
      $install_type = $update['install_type'];
      $release_version_orig = (isset($update['updates']['releases'][0]['orig_version']) ? $update['updates']['releases'][0]['orig_version'] : NULL);
      $release_terms = (isset($update['updates']['releases'][0]['terms']) ? implode(', ', $update['updates']['releases'][0]['terms']) : NULL);

      // Only modules installed with composer
      if ($options['update_string_version'] == TRUE OR $options['update_string'] == TRUE) {
        if ($install_type != 'composer') {
          continue;
        }
      }

      // Transform values.
      if ($update['updates']['security'] == 1) {
        $update['updates']['security'] = 'Yes';
      }
      else {
        $update['updates']['security'] = '';
      }

      if ($update['updates']['insecure'] == 1) {
        $update['updates']['insecure'] = 'Yes';
      }
      else {
        $update['updates']['insecure'] = '';
      }

      $rows[] = array(
        'module' => $update['name'],
        'version' => $update['version'],
        'install_type' => $install_type,
        'composer_path' => $update['composer_path'],
        'composer_setting' => $update['composer_setting'],
        'security' => $update['updates']['security'],
        'insecure' => $update['updates']['insecure'],
        'release_version' => $release_version_orig,
        'release_terms' => $release_terms,
      );

      $composer[] = $update['composer_path'];
      $composer_version[] = $update['composer_path'] . ':' . $release_version_orig;
    }

    if ($options['update_string'] == TRUE) {
      if (count($composer) == 0) {
        return NULL;
      }

      return implode(' ', $composer);
    }

    if ($options['update_string_version'] == TRUE) {
      if (count($composer) == 0) {
        return NULL;
      }

      return implode(' ', $composer_version);
    }

    // Return data as table.
    $data = new RowsOfFields($rows);
    $data->addRendererFunction(
      function ($key, $cellData, FormatterOptions $options, $rowData) {
        if ($key == 'first') {
          return "<comment>$cellData</>";
        }
        return $cellData;
      }
    );
    return $data;
  }
}
