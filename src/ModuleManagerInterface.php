<?php
namespace Drupal\autoupdate;

/**
 * Provides and interface for ModuleManager.
 */
interface ModuleManagerInterface {

  public function getModules($installed = TRUE);
  public function getUpdates($security_only = FALSE, $force_check = FALSE);

}
