
CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Drush commands
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers




INTRODUCTION
------------
USE THIS ON YOUR OWN RISK! You can only use this autoupdate if you use composer for module installation! Drush 9 is also required.

We want to make your Drupal system secure! If you have write access on your webroot and you have a composer.json and composer.lock on your webroot you can use this autoupdate!
You find the settings page for this module here. The module report page, on which you see the updates you can find here.
This module only provides a update checker for each installed module (source drupal.org). You also need a script which executes the drush command.




REQUIREMENTS
------------
If you want to use this module, you need to use composer, drush and a shell script.




RECOMENDED MODULES
-------------------
 * Composer to install drupal modules
 * Drush to execute commands




DRUSH COMMANDS
--------------
To execute a shell script, which updates your Drupal installation, you can use a Drush command.

 * drush autoupdate:updates
    - Get all module updates.
 * drush autoupdate:updates --force
    - Force check for new nupdates.
 * drush autoupdate:updates --security
    - Get only module security updates.
 * drush autoupdate:updates --update_string
    - Get string with modules.
 * drush autoupdate:updates --security --update_string
    - Get string only for security updates.




INSTALLATION
------------
 * Install the module
 * Go to the settings page and setup the configuration
 * Go to the report page and check the updates
   - Check if all works fine, you must see the "version" and "composer setting"
 * Create / copy shell script to your server (outside the webroot)
   - Change the pathes DRUSH / WEBROOT / COMPOSER
   - Test the script manually
   - Setup a crontab




CONFIGURATION
-------------
 * Run the cron once manually configuration page > system > cron
 * Go to the configuration page > system > timetable cron
    - Now you can edit each cron and set there your own time




TROUBLESHOOTING
---------------
If you have issues with autoupdate module, check this steps
 - Please try execute drush command manual
 - Please try execute shell script manual
 - Check if shell script is executable (chmod +x)




MAINTAINERS
-----------
Current maintainers:
  * Ursin Cola (cola) https://www.drupal.org/u/cola

This project has been sponsored by:
 * soul.media
   Drupal agency, www.soul.media
